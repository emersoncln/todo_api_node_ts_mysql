export const todoResponseModel = (id: number, name: string, checked: number) => ({
  id,
  name,
  checked: checked === 1 ?  true :  false
});