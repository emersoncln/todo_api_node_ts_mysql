
import { listAllTodos } from "../interactors/todo/TodoListAllInteractor.ts";
import { deleteTodoById } from "../interactors/todo/TodoDeleteByIdInteractor.ts";
import { updateTodoById } from "../interactors/todo/TodoUpdateByIdInteractor.ts";
import { saveTodo } from "../interactors/todo/TodoInsertInteractor.ts";

import { FastifyReplyType, FastifyRequestType } from "fastify/types/type-provider";

// get, list all todos
const listAll = async () => {
    return await listAllTodos()
}

// delete, delete by id
const deleteById = async (req: FastifyRequestType<any>, resp: FastifyReplyType<any>) => {
    await deleteTodoById(req.params.id, resp)
}

// put, update by id
const updatesById = async (req: FastifyRequestType<any>, resp: FastifyReplyType<any>) => {
    await updateTodoById(req.params.id, req.body, resp)
}

// post, update by id
const insertTodo = async (req: FastifyRequestType<any>, resp: FastifyReplyType<any>) => {
    await saveTodo(req.body, resp)
}

export default {
    listAll, deleteById, updatesById, insertTodo
};