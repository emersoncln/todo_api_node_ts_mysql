import { FastifyReplyType } from "fastify/types/type-provider";
import TodoRepository from "../../persistence/repository/todo/TodoRepository";
import http_status from "../../const/http_status";

export const saveTodo = async (model: any, res: FastifyReplyType<any>) => {
  if (!model.name || model.name.length > 100) {
    return res.status(http_status.CONFLICT).send({
      status: http_status.CONFLICT,
      message: "Nome inválido.",
    });
  }

  try {
    await TodoRepository.insertTodo(model.id, model.name, model.checked);
    return res.status(http_status.CREATED).send();
  } catch (error) {
    return res.status(http_status.INTERNAL_SERVER_ERROR).send({
      status: http_status.INTERNAL_SERVER_ERROR,
      message: "Erro ao tentar salvar o To Do, tente novamente.",
    });
  }
};
