import todoRepository from "../../persistence/repository/todo/TodoRepository";
import { todoResponseModel } from "../../presentation/todo/TodoResponseModel";

export const listAllTodos = async () => {
    const response: any = await todoRepository.findAllTodos();
    return response.map((item: any) => todoResponseModel(item.ID, item.NAME, item.CHECKED));
};
