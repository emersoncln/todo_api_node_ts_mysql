import todoRepository from "../../persistence/repository/todo/TodoRepository";
import { FastifyReplyType } from "fastify/types/type-provider";

import httpStatus  from "../../const/http_status.ts";

export const deleteTodoById = async (
  id: number,
  res: FastifyReplyType<any>
) => {
  const todo: any = await todoRepository.existsTodoById(id);
  if (todo.length > 0) {
    try {
      await todoRepository.deleteTodoById(id);
      return res.status(httpStatus.NO_CONTENT).send();
    } catch (error) {
      return res
        .status(httpStatus.INTERNAL_SERVER_ERROR)
        .send({
          status: httpStatus.INTERNAL_SERVER_ERROR,
          message: "Erro ao tentar deletar o todo, tente novamente.",
        });
    }
  } else {
    return res
      .status(httpStatus.NOT_FOUND)
      .send({ status: httpStatus.NOT_FOUND, message: "To Do não encontrado." });
  }
};
