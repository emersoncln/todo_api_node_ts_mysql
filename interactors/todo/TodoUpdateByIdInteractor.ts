import { FastifyReplyType } from "fastify/types/type-provider";
import TodoRepository from "../../persistence/repository/todo/TodoRepository";
import http_status from "../../const/http_status";

export const updateTodoById = async (
  id: number,
  model: any,
  res: FastifyReplyType<any>
) => {
  if (!model.name || model.name.length > 100) {
    return res.status(http_status.CONFLICT).send({
      status: http_status.CONFLICT,
      message: "Nome inválido.",
    });
  }

  const todo: any = await TodoRepository.existsTodoById(id);
  if (todo.length > 0) {
    try {
      await TodoRepository.updateTodo(id, model.name, model.checked);
      return res.status(http_status.OK).send();
    } catch (error) {
      return res.status(http_status.INTERNAL_SERVER_ERROR).send({
        status: http_status.INTERNAL_SERVER_ERROR,
        message: "Erro ao tentar salvar o todo, tente novamente.",
      });
    }
  } else {
    return res
      .status(http_status.NOT_FOUND)
      .send({ status: http_status.NOT_FOUND, message: "Todo não encontrado." });
  }
};
