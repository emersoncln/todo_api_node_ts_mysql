type TodoResponseType = {
  id: number;
  name: string;
  checked: boolean;
};

type TodoSimpleResponseType = {
  id: number;
  checked: boolean;
};
