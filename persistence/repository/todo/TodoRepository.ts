import connection from "../../../resources/database";

const findAll = "SELECT * FROM TODOS";
const findAllTodos = async () => {
  const [result] = await connection.promise().query(findAll);
  return result;
};

const deleteById = "DELETE FROM TODOS WHERE ID = ?";
const deleteTodoById = async (id: number) => {
  await connection.promise().query(deleteById, [id]);
};

const existsById = "SELECT 1 FROM TODOS WHERE ID = ?";
const existsTodoById = async (id: number) => {
    const [result] = await connection.promise().query(existsById, [id]);
    return result;
};

const insert = "INSERT INTO TODOS (ID, NAME, CHECKED) VALUE ( ?, ?, ?)";
const insertTodo = async (id: number, name: string, checked: boolean ) => {
    const _check = checked ? 1 : 0;
    const _id = id !== -1 ? id :  null;
    await connection.promise().query(insert, [_id, name, _check]);
};

const update = "UPDATE TODOS SET NAME = ?, CHECKED = ? WHERE ID = ?";
const updateTodo = async (id: number, name: string, checked: boolean ) => {
    const _check = checked ? 1 : 0;
    await connection.promise().query(update, [name, _check, id]);
};

export default { findAllTodos, deleteTodoById, existsTodoById, insertTodo, updateTodo };
