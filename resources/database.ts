import mysql from "mysql2";
import "dotenv/config";

const  { DB_PORT, DB_HOST, DB_USER, DB_PASSWORD, DB_DATABASE } = process.env;

const connection = mysql.createConnection({
  host: DB_HOST,
  user: DB_USER,
  password: DB_PASSWORD,
  database: DB_DATABASE,
  port: Number(DB_PORT),
});

connection.connect((error) => {
  if (error) {
    console.log(error.message);
  } else console.log("Connected!");
});

export default connection;
