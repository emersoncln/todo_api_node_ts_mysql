import connection from "../database";

const sql = "CREATE TABLE IF NOT EXISTS TODOS ( ID INT AUTO_INCREMENT PRIMARY KEY, NAME VARCHAR(100)  NOT NULL, CHECKED  BOOLEAN NOT NULL )";
connection.query(sql, (error, _) => {
  if (error) {
    console.log(error.message);
  }
});
