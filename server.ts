import { fastify } from "fastify";
import todoController from "./controllers/todoController.ts";
import cors from '@fastify/cors'

import "dotenv/config";

const  { SERVER_PORT } = process.env;

const options = { port: Number(SERVER_PORT) || 8081 };

const server = fastify();
server.register(cors, { 
    origin: ["*"],
    methods: ["GET", "POST", "PUT", "DELETE"],
    credentials: false
})

// todos routes
server.get("/todos", todoController.listAll);
server.delete("/todos/:id", todoController.deleteById);
server.put("/todos/:id", todoController.updatesById);
server.post("/todos", todoController.insertTodo);

server.listen(options);
